# Copyright (C) 2019-2020  Stephan Kreutzer
#
# This file is part of JStAC.
#
# JStAC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# JStAC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with JStAC. If not, see <http://www.gnu.org/licenses/>.



.PHONY: all JStAC clean



all: JStAC
JStAC: JStAC.class



org/publishing_systems/_20140527t120137z/jstac/CSVStreamException.class: org/publishing_systems/_20140527t120137z/jstac/CSVStreamException.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/CSVStreamException.java

org/publishing_systems/_20140527t120137z/jstac/CSVEvent.class: org/publishing_systems/_20140527t120137z/jstac/CSVEvent.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/CSVEvent.java

org/publishing_systems/_20140527t120137z/jstac/EndLine.class: org/publishing_systems/_20140527t120137z/jstac/EndLine.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/EndLine.java

org/publishing_systems/_20140527t120137z/jstac/Field.class: org/publishing_systems/_20140527t120137z/jstac/Field.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/Field.java

org/publishing_systems/_20140527t120137z/jstac/StartLine.class: org/publishing_systems/_20140527t120137z/jstac/StartLine.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/StartLine.java

org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.class: org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.java

org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.class: org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.java

JStAC.class: JStAC.java org/publishing_systems/_20140527t120137z/jstac/CSVStreamException.class org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.class org/publishing_systems/_20140527t120137z/jstac/CSVEvent.class org/publishing_systems/_20140527t120137z/jstac/StartLine.class org/publishing_systems/_20140527t120137z/jstac/Field.class org/publishing_systems/_20140527t120137z/jstac/EndLine.class org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.class
	javac -encoding UTF-8 JStAC.java

clean:
	rm -f JStAC.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/StartLine.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/Field.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/EndLine.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/CSVEvent.class
	rm -f org/publishing_systems/_20140527t120137z/jstac/CSVStreamException.class
