/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of JStAC.
 *
 * JStAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jstac/Field.java
 * @author Stephan Kreutzer
 * @since 2020-03-08
 */

package org.publishing_systems._20140527t120137z.jstac;



public class Field extends CSVEvent
{
    public Field(String data, long lineNumber, int fieldNumber)
    {
        /** @todo Consider a boolean hasNext parameter. */

        if (data == null)
        {
            throw new IllegalArgumentException();
        }

        this.data = data;
        this.lineNumber = lineNumber;
        this.fieldNumber = fieldNumber;
    }

    public String getData()
    {
        return this.data;
    }

    public long getLineNumber()
    {
        return this.lineNumber;
    }

    public int getFieldNumber()
    {
        return this.fieldNumber;
    }

    protected String data = null;
    protected long lineNumber;
    protected int fieldNumber;
}
