/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of JStAC.
 *
 * JStAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jstac/CSVEvent.java
 * @author Stephan Kreutzer
 * @since 2020-03-07
 */

package org.publishing_systems._20140527t120137z.jstac;



public class CSVEvent
{
    public CSVEvent()
    {

    }

    public boolean isStartLine()
    {
        return this instanceof StartLine;
    }

    public StartLine asStartLine()
    {
        return (StartLine)this;
    }

    public boolean isField()
    {
        return this instanceof Field;
    }

    public Field asField()
    {
        return (Field)this;
    }

    public boolean isEndLine()
    {
        return this instanceof EndLine;
    }

    public EndLine asEndLine()
    {
        return (EndLine)this;
    }
}
