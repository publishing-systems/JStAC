/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of JStAC.
 *
 * JStAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jstac/CSVEventReader.java
 * @brief Parses a CSV file.
 * @author Stephan Kreutzer
 * @since 2020-03-07
 */

package org.publishing_systems._20140527t120137z.jstac;



import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.Queue;
import java.util.LinkedList;



public class CSVEventReader
{
    public CSVEventReader(InputStream stream, String delimiter, boolean ignoreFirstLine) throws CSVStreamException
    {
        this.delimiter = delimiter;
        this.ignoreFirstLine = ignoreFirstLine;

        try
        {
            this.reader = new BufferedReader(
                          new InputStreamReader(stream, "UTF-8"));
        }
        catch (UnsupportedEncodingException ex)
        {
            throw new CSVStreamException(ex);
        }
    }

    /**
     * @details The original doesn't throw a CSVStreamException here because it doesn't
     *     read ahead + buffering, but instead only checks the stream for EOL and then
     *     reads in nextEvent(), potentially throwing the exception there.
     */
    public boolean hasNext() throws CSVStreamException
    {
        if (this.events.size() > 0)
        {
            return true;
        }

        if (this.hasNextCalled == true)
        {
            return false;
        }
        else
        {
            this.hasNextCalled = true;
        }


        if (this.ignoreFirstLine == true)
        {
            int result = HandleLine(this.ignoreFirstLine);

            if (result == this.RETURN_HANDLELINE_ENDOFLINE)
            {
                // Successfully omitted.
            }
            else if (result == RETURN_HANDLELINE_OUTOFCHARACTERS)
            {
                return false;
            }
            else if (result == RETURN_HANDLELINE_NOCHARACTERS)
            {
                return false;
            }
            else
            {
                throw new UnsupportedOperationException();
            }

            this.ignoreFirstLine = false;
        }

        while (true)
        {
            int result = HandleLine(false);

            if (result == this.RETURN_HANDLELINE_ENDOFFIELD)
            {
                break;
            }
            if (result == this.RETURN_HANDLELINE_ENDOFLINE)
            {
                break;
            }
            else if (result == this.RETURN_HANDLELINE_OUTOFCHARACTERS)
            {
                break;
            }
            else if (result == this.RETURN_HANDLELINE_NOCHARACTERS)
            {
                break;
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }

        return this.events.size() > 0;
    }

    protected static final int RETURN_HANDLELINE_ENDOFFIELD = 0;
    protected static final int RETURN_HANDLELINE_ENDOFLINE = 1;
    protected static final int RETURN_HANDLELINE_OUTOFCHARACTERS = 2;
    protected static final int RETURN_HANDLELINE_NOCHARACTERS = 3;

    protected int HandleLine(boolean omitOutput) throws CSVStreamException
    {
        this.countDelimiterMatch = 0;
        this.countCharacter = 0L;

        while (true)
        {
            int result = HandleField(omitOutput);

            if (result == this.RETURN_HANDLEFIELD_ENDOFFIELD)
            {
                return this.RETURN_HANDLELINE_ENDOFFIELD;
            }
            else if (result == this.RETURN_HANDLEFIELD_ENDOFLINE ||
                     result == this.RETURN_HANDLEFIELD_OUTOFCHARACTERS)
            {
                if (this.countFieldMax < 0)
                {
                    this.countFieldMax = this.countField;
                }
                else
                {
                    if (this.countField != this.countFieldMax)
                    {
                        throw new CSVStreamException("Irregular field count " + this.countField + " while a count of " + this.countFieldMax + " was expected on line " + (this.countLine + 1L) + ".");
                    }
                }

                this.countField = 0;

                if (omitOutput == false)
                {
                    this.events.add(new EndLine(this.countLine + 1));
                }

                ++this.countLine;

                if (result == this.RETURN_HANDLEFIELD_ENDOFLINE)
                {
                    return this.RETURN_HANDLELINE_ENDOFLINE;
                }
                else if (result == this.RETURN_HANDLEFIELD_OUTOFCHARACTERS)
                {
                    return this.RETURN_HANDLELINE_OUTOFCHARACTERS;
                }
                else
                {
                    throw new UnsupportedOperationException();
                }
            }
            else if (result == this.RETURN_HANDLEFIELD_NOCHARACTERS)
            {
                if (this.countFieldMax < 0)
                {
                    // First line and EOF as the first field.
                    return this.RETURN_HANDLELINE_NOCHARACTERS;
                }
                else
                {
                    if (this.countField <= 0)
                    {
                        // CRLF at the end of previous line, then EOF
                        // as the first field, valid according to the
                        // RFC and shouldn't lead to an exception because
                        // of irregularity, shouldn't lead to a StartLine
                        // with a first empty field in it.
                        return this.RETURN_HANDLELINE_NOCHARACTERS;
                    }
                    else
                    {
                        // Should never happen, RETURN_HANDLEFIELD_NOCHARACTERS did check for
                        // this.countField <= 0.
                        throw new UnsupportedOperationException();
                    }
                }
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }
    }

    protected static final int RETURN_HANDLEFIELD_ENDOFFIELD = 0;
    protected static final int RETURN_HANDLEFIELD_ENDOFLINE = 1;
    protected static final int RETURN_HANDLEFIELD_OUTOFCHARACTERS = 2;
    protected static final int RETURN_HANDLEFIELD_NOCHARACTERS = 3;

    protected int HandleField(boolean omitOutput) throws CSVStreamException
    {
        this.countDelimiterMatch = 0;

        int character = -1;

        try
        {
            character = this.reader.read();
        }
        catch (IOException ex)
        {
            throw new CSVStreamException(ex);
        }

        if (character < 0 &&
            this.countField <= 0)
        {
            return this.RETURN_HANDLEFIELD_NOCHARACTERS;
        }

        this.countCharacter = 0L;

        if (this.countField <= 0 &&
            omitOutput == false)
        {
            this.events.add(new StartLine(this.countLine + 1));
        }

        StringBuilder sb = new StringBuilder();

        while (character >= 0)
        {
            ++this.countCharacter;

            if (character == '\r')
            {
                if (this.countDelimiterMatch > 0)
                {
                    if (omitOutput == false)
                    {
                        sb.append(this.delimiter.substring(0, this.countDelimiterMatch));
                    }

                    this.countDelimiterMatch = 0;
                }

                try
                {
                    character = this.reader.read();
                }
                catch (IOException ex)
                {
                    throw new CSVStreamException(ex);
                }

                if (character == '\n')
                {
                    if (omitOutput == false)
                    {
                        this.events.add(new Field(sb.toString(), this.countLine + 1, this.countField + 1));
                    }

                    ++this.countField;

                    return this.RETURN_HANDLEFIELD_ENDOFLINE;
                }
                else
                {
                    if (omitOutput == false)
                    {
                        sb.append('\r');
                    }

                    continue;
                }
            }
            else if (character == this.delimiter.charAt(this.countDelimiterMatch))
            {
                ++this.countDelimiterMatch;

                if (this.countDelimiterMatch == this.delimiter.length())
                {
                    if (omitOutput == false)
                    {
                        this.events.add(new Field(sb.toString(), this.countLine + 1, this.countField + 1));
                    }

                    this.countDelimiterMatch = 0;
                    ++this.countField;

                    return this.RETURN_HANDLEFIELD_ENDOFFIELD;
                }
            }
            else if (character == '"')
            {
                if (this.countCharacter == 1L)
                {
                    return HandleFieldEnclosed(omitOutput);
                }
                else
                {
                    if (this.countDelimiterMatch > 0)
                    {
                        if (omitOutput == false)
                        {
                            sb.append(this.delimiter.substring(0, this.countDelimiterMatch));
                        }

                        this.countDelimiterMatch = 0;
                    }

                    if (omitOutput == false)
                    {
                        sb.append((char)character);
                    }
                }
            }
            else
            {
                if (this.countDelimiterMatch > 0)
                {
                    if (omitOutput == false)
                    {
                        sb.append(this.delimiter.substring(0, this.countDelimiterMatch));
                    }

                    this.countDelimiterMatch = 0;
                }

                if (omitOutput == false)
                {
                    sb.append((char)character);
                }
            }

            try
            {
                character = this.reader.read();
            }
            catch (IOException ex)
            {
                throw new CSVStreamException(ex);
            }
        }

        if (omitOutput == false)
        {
            this.events.add(new Field(sb.toString(), this.countLine + 1, this.countField + 1));
        }

        ++this.countField;

        return this.RETURN_HANDLEFIELD_OUTOFCHARACTERS;
    }

    protected int HandleFieldEnclosed(boolean omitOutput) throws CSVStreamException
    {
        int character = -1;

        try
        {
            character = this.reader.read();
        }
        catch (IOException ex)
        {
            throw new CSVStreamException(ex);
        }

        StringBuilder sb = new StringBuilder();

        while (character >= 0)
        {
            ++this.countCharacter;

            if (character == '"')
            {
                try
                {
                    character = this.reader.read();
                }
                catch (IOException ex)
                {
                    throw new CSVStreamException(ex);
                }

                if (character < 0)
                {
                    throw new CSVStreamException("Unexpected end of stream while trying to parse an enclosed field on line " + (this.countLine + 1L) + ".");
                }

                ++this.countCharacter;

                if (character == '"')
                {
                    if (omitOutput == false)
                    {
                        sb.append((char)character);
                    }
                }
                else
                {
                    if (omitOutput == false)
                    {
                        this.events.add(new Field(sb.toString(), this.countLine + 1, this.countField + 1));
                    }

                    while (true)
                    {
                        if (character == '\r')
                        {
                            if (this.countDelimiterMatch > 0)
                            {
                                throw new CSVStreamException("Carriage return character encountered while matching the field delimiter after the end of an enclosed field on line " + (this.countLine + 1L) + ".");
                            }

                            try
                            {
                                character = this.reader.read();
                            }
                            catch (IOException ex)
                            {
                                throw new CSVStreamException(ex);
                            }

                            if (character < 0)
                            {
                                throw new CSVStreamException("Unexpected end of stream while trying to parse an enclosed field on line " + (this.countLine + 1L) + ".");
                            }

                            ++this.countCharacter;

                            if (character == '\n')
                            {
                                ++this.countField;

                                return this.RETURN_HANDLEFIELD_ENDOFLINE;
                            }
                            else
                            {
                                throw new CSVStreamException("Carriage return character wasn't followed up by a line feed character while trying to parse an enclosed field on line " + (this.countLine + 1L) + ".");
                            }
                        }
                        else if (character == this.delimiter.charAt(this.countDelimiterMatch))
                        {
                            ++this.countDelimiterMatch;

                            if (this.countDelimiterMatch == this.delimiter.length())
                            {
                                this.countDelimiterMatch = 0;
                                ++this.countField;

                                return this.RETURN_HANDLEFIELD_ENDOFFIELD;
                            }
                        }
                        else
                        {
                            throw new CSVStreamException("Illegal character '" + character + "' after the closing quotation mark of an enclosed field on line " + (this.countLine + 1L) + ".");
                        }

                        try
                        {
                            character = this.reader.read();
                        }
                        catch (IOException ex)
                        {
                            throw new CSVStreamException(ex);
                        }

                        if (character < 0)
                        {
                            throw new CSVStreamException("Unexpected end of stream while trying to parse an enclosed field on line " + (this.countLine + 1L) + ".");
                        }

                        ++this.countCharacter;
                    }
                }
            }
            else
            {
                if (omitOutput == false)
                {
                    sb.append((char)character);
                }
            }

            try
            {
                character = this.reader.read();
            }
            catch (IOException ex)
            {
                throw new CSVStreamException(ex);
            }
        }

        throw new CSVStreamException("Unexpected end of stream while trying to parse an enclosed field on line " + (this.countLine + 1L) + ".");
    }

    public CSVEvent nextEvent() throws CSVStreamException
    {
        if (this.events.size() <= 0 &&
            this.hasNextCalled == false)
        {
            if (hasNext() != true)
            {
                throw new CSVStreamException("Attempted CSVEventReader.nextEvent() while there isn't one instead of checking CSVEventReader.hasNext() first.");
            }
        }

        this.hasNextCalled = false;

        if (this.events.size() <= 0)
        {
            throw new CSVStreamException("CSVEventReader.nextEvent() while there isn't one, ignoring CSVEventReader.hasNext() == false.");
        }

        return this.events.poll();
    }


    String delimiter = null;
    boolean ignoreFirstLine = false;

    protected int countDelimiterMatch = 0;
    protected long countLine = 0L;
    protected int countFieldMax = -1;
    protected int countField = 0;

    protected long countCharacter = 0L;

    protected BufferedReader reader = null;
    protected boolean hasNextCalled = false;
    protected Queue<CSVEvent> events = new LinkedList<CSVEvent>();
}
