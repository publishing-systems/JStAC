/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of JStAC.
 *
 * JStAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jstac/CSVInputFactory.java
 * @author Stephan Kreutzer
 * @since 2020-03-07
 */

package org.publishing_systems._20140527t120137z.jstac;



import java.io.InputStream;



public class CSVInputFactory
{
    protected CSVInputFactory()
    {

    }

    public static CSVInputFactory newInstance()
    {
        return new CSVInputFactory();
    }

    public CSVEventReader createCSVEventReader(InputStream stream) throws CSVStreamException
    {
        return new CSVEventReader(stream, this.delimiter, this.ignoreFirstLine);
    }

    public int setDelimiter(String delimiter)
    {
        if (delimiter.length() <= 0)
        {
            throw new IllegalArgumentException("Delimiter is an empty string.");
        }

        if (delimiter.indexOf('\"') >= 0 ||
            delimiter.indexOf('\r') >= 0 ||
            delimiter.indexOf('\n') >= 0)
        {
            throw new IllegalArgumentException("Delimiter contains a reserved CSV character.");
        }

        this.delimiter = delimiter;

        return 0;
    }

    public int setIgnoreFirstLine(boolean ignore)
    {
        this.ignoreFirstLine = ignore;
        return 0;
    }

    protected String delimiter = ",";
    protected boolean ignoreFirstLine = false;
}
