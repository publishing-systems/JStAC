/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of JStAC.
 *
 * JStAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JStAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JStAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JStAC.cpp
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2020-03-07
 */



import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import org.publishing_systems._20140527t120137z.jstac.CSVInputFactory;
import org.publishing_systems._20140527t120137z.jstac.CSVEventReader;
import org.publishing_systems._20140527t120137z.jstac.CSVEvent;
import org.publishing_systems._20140527t120137z.jstac.StartLine;
import org.publishing_systems._20140527t120137z.jstac.Field;
import org.publishing_systems._20140527t120137z.jstac.EndLine;
import org.publishing_systems._20140527t120137z.jstac.CSVStreamException;



public class JStAC
{
    public static void main(String args[])
    {
        System.out.print("JStAC Copyright (C) 2020 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/JStAC/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        try
        {
            if (args.length > 0)
            {
                File inputFile = new File(args[0]);

                if (inputFile.exists() != true)
                {
                    System.out.println("File '" + args[0] + "' doesn't exist.");
                    return;
                }

                if (inputFile.isFile() != true)
                {
                    System.out.println("Path '" + args[0] + "' isn't a file.");
                    return;
                }

                if (inputFile.canRead() != true)
                {
                    System.out.println("File '" + args[0] + "' isn't readable.");
                    return;
                }

                InputStream in = new FileInputStream(inputFile);

                Run(in);
            }
        }
        catch (Exception ex)
        {
            System.out.println("Exception: " + ex.getMessage());
            return;
        }
    }

    public static int Run(InputStream stream) throws CSVStreamException
    {
        CSVInputFactory inputFactory = CSVInputFactory.newInstance();
        CSVEventReader eventReader = inputFactory.createCSVEventReader(stream);

        while (eventReader.hasNext() == true)
        {
            CSVEvent event = eventReader.nextEvent();

            if (event.isStartLine() == true)
            {
                StartLine startLine = event.asStartLine();
            }
            else if (event.isField() == true)
            {
                Field field = event.asField();

                if (field.getFieldNumber() > 1)
                {
                    System.out.print(",");
                }

                System.out.print("\"");
                System.out.print(field.getData().replaceAll("\"", "\"\""));
                System.out.print("\"");
            }
            else if (event.isEndLine() == true)
            {
                EndLine endLine = event.asEndLine();

                System.out.print("\r\n");
            }
        }

        return 0;
    }
}
